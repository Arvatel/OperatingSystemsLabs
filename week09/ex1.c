#include <stdio.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>


struct page
{
    int id;
    int counter;
};

int main()
{
 
    FILE *inp;
    inp = fopen("ex1_input.txt", "r");
    // inp = fopen("input", "r"); // for ex2
    int n_p_pages;
    int hit = 0;
    int miss = 0;
    int s_page;

    struct page * p_mem;

    printf("Please write the number of pages you have in main fake memory:\n");
    scanf("%d", &n_p_pages);
    
    int r_bit[n_p_pages];
    p_mem = malloc(sizeof(struct page) * n_p_pages);

    for (int i = 0; i < n_p_pages; i++){
        p_mem[i].id = -1;
        p_mem[i].counter = 0;

        r_bit[i] = 0;
    }


    // printf("Note that the sequence should end with EOF\n");
    // printf("In bash inputing sequence can be ended using ctrl+d combination\n");

    while(fscanf(inp, "%d", &s_page) != EOF)
    {
        //Print as in example;
        for (int j = 0; j < n_p_pages; j++)
        {
            printf("i = %d, id = %d, counter = %d\n", j, p_mem[j].id, p_mem[j].counter);
        }
        printf("Current page: %d\n\n", s_page);

        //set initial values
        int lowest_i = -1;
        int lowest_count = INT_MAX;
        bool flag = false; // if miss - false, if true - hit 
        bool fix = false; // if we have empty space in memory

        //checking hit or miss
        for (int j = 0; j < n_p_pages; j++)
        {
            if (s_page == p_mem[j].id)
            {
                r_bit[j] = 1;

                flag = true;
                break;
            }
            else
            {
                if (p_mem[j].counter <= lowest_count)
                {
                    if (fix) continue;
                    if (p_mem[j].id == -1){
                        fix = true;
                        lowest_count = -1;
                        lowest_i = j;
                    } 
                    lowest_count = p_mem[j].counter;
                    lowest_i = j;
                }
            }
        }

        if (flag) hit++;
        else
        {
            miss++;
            p_mem[lowest_i].id = s_page;
            p_mem[lowest_i].counter = 0;

            r_bit[lowest_i] = 1;
        }

        // updating counters
        for (int j = 0; j < n_p_pages; j++)
        {
            p_mem[j].counter = (p_mem[j].counter >> 1) | (r_bit[j] << 15);
        }


        //reset r_bit's to 0 after the tick
        for (int j = 0; j < n_p_pages; j++){
            r_bit[j] = 0;
        }
    }

    printf("hit = %d, miss = %d, hit/miss ratio: %.4f\n", hit, miss, ((float)hit)/miss);

    fclose(inp);
    return 0;
}