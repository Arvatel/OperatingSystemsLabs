#include <stdio.h>
#include <pthread.h>


void* start_routine (void * g)
{
    printf("I am thread %d\n", *((int*)g));
    fflush(stdout);
}


int main()
{

    int n;
    int tid;
    scanf ("%d",&n);
    int i = 0;
    while (i < n)
    {
        pthread_t tid;
        printf("Creating thread %d\n", i);
        fflush(stdout);
        pthread_create(&tid, NULL, start_routine, ((void*)&i));
        void* val;
        pthread_join(tid, &val);
        i++;
    }
    
    return 0;
}
