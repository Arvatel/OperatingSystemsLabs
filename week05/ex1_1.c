#include <stdio.h>
#include <pthread.h>


void* start_routine (void * g)
{
    printf("I am thread %d\n", ((int*)g));
}


int main()
{
    int n;
    scanf ("%d",&n);
    int i = 0;
    while (i < n)
    {
        pthread_t inc_x_thread;
        printf("Creating thread %d\n", i);
        pthread_create(&inc_x_thread, NULL, start_routine, (void*)i);
        i++;
    }
    
    return 0;
}
