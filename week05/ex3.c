#include <stdio.h>
#include <pthread.h>

#define MAX_SIZE 1000

int buff[MAX_SIZE];
int delta;
int prin;

int slp[2]; //0- produser 1- consumer

void sleep(int i)
{
    while (slp[i]){}
    slp[i] = 1;
}

/**
* Race condition occurs here (and in places where this function calls)
* Because one tells wake up in a moment when oter wants to go to sleep but still awake
*/
void wakeup(int i) 
{
    slp[i] = 0;  
}

void* producer_routine (void * g)
{
    int wakeU = 0;
    prin = 0;
    while (0 == 0)
    {
        if (delta == MAX_SIZE)
            sleep(0); //sleep?
        
        if (delta == 0)
            wakeU = 1;
        else 
            wakeU = 0;
            
        buff[delta] = 1;
        delta = delta + 1;
        
        if (wakeU)
            wakeup(1); //Race condition
        
        prin = (prin + 1) % 51;
        if (prin == 0)
            printf("I am here!\n");
    }
}


void* consumer_routine (void * g)
{
    int ful;
    while (0 == 0)
    {
        if (delta == 0)
            sleep(1); //sleep?
       
        ful = delta == (MAX_SIZE - 1);
            
        buff[delta] = 2; //replace 1 with 2
        delta = delta - 1;
        
        if (ful)
            wakeup(0); //Race condition
    }
}


int main()
{
    delta = 0;
    slp[0] = 1;
    slp[1] = 1;
    
    pthread_t produser_thread;
    pthread_t consumer_thread;

    pthread_create(&produser_thread, NULL, producer_routine , NULL);
    consumer_routine(NULL);
    
    return 0;
}
