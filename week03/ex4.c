#include <stdio.h>

void swap(int* a, int* b){
	
	int temp = *a;
	*a = *b;
	*b = temp;
}

int pivot_fun(int* a, int left, int right){

	int pivot = a[right];
	int i = left - 1;

	for (int j = left; j <= right; j++){
		if (a[j] < pivot){
			i++;
			swap(&a[i], &a[j]);
		}
	}
	swap(&a[i + 1], &a[right]);

	return (i + 1);
}

void quick_sort(int* a, int left, int right){

	if (left < right){
		int pivot = pivot_fun(a, left, right);

		quick_sort(a, left, pivot - 1);
		quick_sort(a, pivot + 1, right);
	}
}

int main(){

	int n;
	printf("Enter size of the array:\n");
	scanf("%d", &n);

	int a[n];

	printf("Enter your array: \n");

	for (int i = 0; i < n; i++){
		scanf("%d", &a[i]);
	}

	printf("\n");
	printf("Array before sorting: \n");

	for (int i = 0; i < n; i++){
		printf("%d ", a[i]);
	}
	printf("\n");

	quick_sort(a, 0, n - 1);

	printf("Sorted array: \n");

	for (int i = 0; i < n; i++){
		printf("%d ", a[i]);
	}
	printf("\n");

	return 0;
}