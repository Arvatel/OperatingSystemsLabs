#include <stdio.h>

void swap(int* a, int* b){
	
	int temp = *a;
	*a = *b;
	*b = temp;
}

void bubble_sort(int* a, int n) {

	for (int i = 0; i < n-1; i++){
		for (int j = 0; j < n - i - 1; j++){
			if (a[j] > a[j + 1]){
				swap(&a[j], &a[j + 1]);
			}
		}
	}
}


int main() {

	int n;
	printf("Enter size of the array:\n");
	scanf("%d", &n);

	int a[n];

	printf("Enter your array: \n");

	for (int i = 0; i < n; i++){
		scanf("%d", &a[i]);
	}

	printf("\n");
	printf("Array before sorting: \n");

	for (int i = 0; i < n; i++){
		printf("%d ", a[i]);
	}
	printf("\n");

	bubble_sort(a, n);

	printf("Sorted array: \n");

	for (int i = 0; i < n; i++){
		printf("%d ", a[i]);
	}
	printf("\n");

	return 0;
}