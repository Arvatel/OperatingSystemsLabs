#include <stdio.h>

int main(){

	int n;
	scanf("%d", &n);

	for (int i = 0; i < n; i++){

		for (int k = 0; k < n - i; k++){
			printf("%c", ' ');
		}

		printf("%c", '*');

		for (int k = 0; k < i; k++){
			printf("%c", '*');
			printf("%c", '*');
		}

		// for (int k = 0; k < n - i; k++){
		// 	printf("%c", ' ');
		// }
		printf("\n");
	}

	return 0;
}