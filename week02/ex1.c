#include <stdio.h>
#include <float.h>
#include <limits.h>


int main(){

	int one = INT_MAX;
	float two = FLT_MAX;
	double three = DBL_MAX;

	printf("Integer size: %zu, number: %d\n", sizeof(one), one);
	printf("Float size: %zu, number: %f\n", sizeof(two), two);
	printf("Double size: %zu, number: %lf\n", sizeof(three), three);

	return 0;
}