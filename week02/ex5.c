#include <stdio.h>


void rightSideUpTriangle(int *n){

	for (int i = 0; i < *n; i++){

		for (int k = 0; k < *n - i; k++){
			printf("%c", ' ');
		}

		printf("%c", '*');

		for (int k = 0; k < i; k++){
			printf("%c", '*');
			printf("%c", '*');
		}

		printf("\n");
	}
}

void rightAngleTriangle(int *n){

	for (int i = 1; i <= *n; i++){

		for (int k = 0; k < i; k++){
			printf("%c", '*');
		}

		printf("\n");
	}
}

void triangle(int *n){

	for (int i = 1; i <= *n; i++){

		if (i <= (*n + 1) / 2){

			for (int k = 0; k < i; k++){
				printf("%c", '*');
			}

		}
		else {
			
			for (int k = 0; k <= *n - i; k++){
				printf("%c", '*');
			}
		}

		printf("\n");
	}
}

void square(int *n){

	for (int i = 0; i < *n; i++){

		for (int k = 0; k < *n; k++){
			printf("%c", '*');
		}

		printf("\n");
	}
}



void choose(int *c, int *n){
	if (*c == 0){
		rightSideUpTriangle(n);
	}

	if (*c == 1){
		rightAngleTriangle(n);
	}

	if (*c == 2){
		triangle(n);
	}

	if (*c == 3){
		square(n);
	}
}



int main(){

	int n = 5, c = 0;


	printf("Hello!\nYou can choose figure\n");
	printf(" 0:\n");
	choose(&c, &n);
	printf("\n");

	c++;
	printf(" 1:\n");
	choose(&c, &n);
	printf("\n");

	c++;
	printf(" 2:\n");
	choose(&c, &n);
	printf("\n");

	c++;
	printf(" 3:\n");
	choose(&c, &n);
	printf("\n");

	printf("Write type of a figure (number): \n");
	scanf("%d", &c);

	printf("Write size: \n");
	scanf("%d", &n);
	

	choose(&c, &n);
	

	return 0;
}