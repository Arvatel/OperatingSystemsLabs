#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include "/usr/include/linux/input.h" 
#include <fcntl.h>


int main() {
    
    // pci-0000:06:00.3-usb-0:4:1.0-event-kbd - it is file in my sysnem with works, 
    // if you want to to try to run this code you should chose another line

    // int input = open("/dev/input/by-path/pci-0000:06:00.3-usb-0:4:1.0-event-kbd", O_RDONLY);
    int input = open("/dev/input/by-path/platform-i8042-serio-0-event-kbd", O_RDONLY);

    FILE *output =  fopen("ex3.txt", "w");

    if (input == -1) {
        printf("Failed to open");
        return 0;
    }

    fcntl(input, F_SETFL, 0);

    struct input_event ie;

    int pressed[256];

    for (int i = 0; i < 256; i++) {
        pressed[i] = 0;
    }

    while (1) {
        int sizeofRed;
        if (read(input, (void*) &ie, sizeof(struct input_event)) < sizeof(struct input_event)) {
            continue;
        }

        if (ie.value == EV_KEY) {
            pressed[ie.code] = 1;
        }

        if (ie.value == EV_REL) {
            pressed[ie.code] = 0;
        }

        if (pressed[KEY_P] && pressed[KEY_E]) {
            fprintf(output, "I passed Exam!\n");
        }

        if (pressed[KEY_C] && pressed[KEY_A] && pressed[KEY_P]) {
            fprintf(output, "Get some cappuccino!\n");
        }

        if (pressed[KEY_Z] && pressed[KEY_X] && pressed[KEY_V]) {
            fprintf(output, "pci-0000:06:00.3-usb-0:4:1.0-event-kbd\n");
        }

        fflush(output);

    }
}