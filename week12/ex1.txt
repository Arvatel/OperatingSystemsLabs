0000-0cf7 : PCI Bus 0000:00
  0000-001f : dma1
  0020-0021 : pic1
  0040-0043 : timer0
  0050-0053 : timer1
  0060-0060 : keyboard
  0061-0061 : PNP0800:00
  0062-0062 : PNP0C09:00
    0062-0062 : EC data
  0064-0064 : keyboard
  0066-0066 : PNP0C09:00
    0066-0066 : EC cmd
  0070-0071 : rtc0
  0080-008f : dma page reg
  00a0-00a1 : pic2
  00c0-00df : dma2
  00f0-00ff : fpu
    00f0-00fe : PNP0C04:00
  0400-0403 : ACPI PM1a_EVT_BLK
  0404-0405 : ACPI PM1a_CNT_BLK
  0408-040b : ACPI PM_TMR
  0420-0427 : ACPI GPE0_BLK
  0480-048f : PNP0C09:00
  04d0-04d1 : pnp 00:03
  04d6-04d6 : pnp 00:03
  0800-0800 : ACPI PM2_CNT_BLK
  0c00-0c01 : pnp 00:03
  0c14-0c14 : pnp 00:03
  0c50-0c52 : pnp 00:03
  0c6c-0c6c : pnp 00:03
  0c6f-0c6f : pnp 00:03
  0cd0-0cdb : pnp 00:03
0cf8-0cff : PCI conf1
0d00-ffff : PCI Bus 0000:00
  1000-1fff : PCI Bus 0000:06
    1000-10ff : 0000:06:00.0
  2000-2fff : PCI Bus 0000:03
    2000-20ff : 0000:03:00.0
  3000-3fff : PCI Bus 0000:01
    3000-307f : 0000:01:00.0
  ff00-ff08 : piix4_smbus
  ff20-ff28 : piix4_smbus


DMA (dma1) - Direct Memory Access - way to get direct access to memory without CPU and the method of data exchange between devices - PIO (Programmed Input / Output)

Timer (timer1) - this timer (exactly timer 1) counts only forward, and it is not reset on its own (without the help of triggers from other modules) when comparing its counter with a certain value, but is reset only after the end of the count

pic1 - programmable interrupt controller. Helps the CPU handle direct interrupts from DMA or other devices that use interrupts to communicate with the system