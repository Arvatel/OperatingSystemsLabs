#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include "/usr/include/linux/input.h" 
#include <fcntl.h>


int main() {

    // pci-0000:06:00.3-usb-0:4:1.0-event-kbd - it is file in my sysnem with works, 
    // if you want to to try to run this code you should chose another line

    // int input = open("/dev/input/by-path/pci-0000:06:00.3-usb-0:4:1.0-event-kbd", O_RDONLY);
    int input = open("/dev/input/by-path/platform-i8042-serio-0-event-kbd", O_RDONLY);


    FILE *output =  fopen("ex2.txt", "w");

    if (input == -1) {
        printf("Failed to open");
        return 0;
    }

    fcntl(input, F_SETFL, 0);

    struct input_event ie;

    while (true) {
        int sizeofRed;
        if (read(input, (void*) &ie, sizeof(struct input_event)) < sizeof(struct input_event)) {
            continue;
        }
        if (ie.value == EV_KEY) {
            fprintf(output, "PRESSED - %x (%d)\n", ie.code, ie.code);
            fflush(output);
        }
        if (ie.value == EV_REL) {
            fprintf(output, "RELEASED - %x (%d)\n", ie.code, ie.code);
            fflush(output);
        }
    }
}