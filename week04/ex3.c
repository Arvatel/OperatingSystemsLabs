#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main(){

    char buff[1000];

    printf("Please don't use 'sudo su' and comands like this,\n");
    printf(" because it breaks fgets and the terminal will be stuck :(\n");
    printf("\nPrint \"exit\" for exit from program MyBash\n");

    while (strcmp(buff, "exit\n")){

        printf("[user@User]$ ");

        fgets(buff, sizeof(buff), stdin);
        
        int size = strlen(buff);

        if (buff[size - 1] == '&'){

            buff[size - 1] = '\0';

            if (fork() == 0){
                system(buff);
            }
        }
        else {
            system(buff);
        }
    }
    
    return 0;
} 
