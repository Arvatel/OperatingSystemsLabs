#include <stdio.h>
#include <sys/types.h>


void parse(char *input, char **commands){

    while (*input != '\0'){
        while (*input == ' ' || *input == '\t' || *input == '\n'){
            *input++ = '\0'; // replace spaces with 0
        }

        *commands++ = input;

        while (*input != '\0' && *input != '\t' && *input != '\n' && *input != ' ') {
            input++;
        }
    }

    *commands = '\0'; // end of arguments
}

void execute(char **commands){

    pid_t pid;
    int status;
     
    // fork child process
    if ((pid = fork()) < 0) { 
        printf(">>>> ERROR: forking child process failed\n");
        exit(1);
    }
    // execute child process
    else if (pid == 0) {
        if (execvp(*commands, commands) < 0) {
           printf(">>>> ERROR: execution failed\n");
           exit(1);
        }
    }

    else {
        while (wait(&status) != pid);
    }
}



int main(){

    char input[1000];
    char *commands[128];

    // printf("Please don't use 'sudo su' and comands like this,\n");
    // printf(" because it breaks fgets and the terminal will be stuck :(\n");
    // printf("\nPrint \"exit\" for exit from program MyBash\n");

    while (1){
        
        printf("[user@User]$ ");

        gets(input);

        parse(input, commands);

        if (strcmp(commands[0], "exit") == 0){
            exit(0);
        }

        execute(commands);
    }


    return 0;
} 
